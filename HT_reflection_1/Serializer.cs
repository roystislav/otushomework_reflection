﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HT_reflection_1
{
    public class Serializer
    {
        public StringBuilder SerializeToCSV<T>(T obj)
        {
            StringBuilder CSVdata = new StringBuilder();
            Type type = obj.GetType();
            var fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic);

            foreach (var field in fields)
            {
                var x = field.Name + ":" + field.GetValue(obj) + ",";
                CSVdata.Append(x);
            }

            CSVdata.Remove(CSVdata.Length - 1, 1);

            return CSVdata;          

        }

        public static T DeserializeFromCSV<T>(string csv)
        {
            FieldInfo field;
            Type t = typeof(T);
            var obj = t.GetConstructor(new Type[] { }).Invoke(new object[] { });
            var proplist = csv.Split(',');
            string[] prop;

            foreach (string s in proplist)
            {
                prop = s.Split(":");
                if (s == "")
                    break;
                field = t.GetField(prop[0], BindingFlags.Instance | BindingFlags.NonPublic);
                switch (field.FieldType.Name)
                {
                    case "Int32":
                        field.SetValue(obj, Convert.ToInt32(prop[1]));
                        break;
                    default:
                        field.SetValue(obj, prop[1]);
                        break;
                }
            }

            return (T)obj;
        }

    }
}
