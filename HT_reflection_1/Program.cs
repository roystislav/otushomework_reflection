﻿using System;
using System.Diagnostics;
using System.Text;
using Newtonsoft.Json;


namespace HT_reflection_1
{
    class Program
    {
        public class F
        {
            [JsonProperty]
            int i1, i2, i3, i4, i5;
            public static F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };

            public void ShowFields()
            {
                Console.WriteLine("i1: {0}", i1);
                Console.WriteLine("i2: {0}", i2);
                Console.WriteLine("i3: {0}", i3);
                Console.WriteLine("i4: {0}", i4);
                Console.WriteLine("i5: {0}", i5);
            }
        }

        static void Main(string[] args)
        {

            F instanceF = F.Get();

            Serializer serializer = new Serializer();
            Stopwatch stopwatch = new Stopwatch();
            StringBuilder serialized = new StringBuilder();

            stopwatch.Start();

            for (int i = 0; i < 100; i++)
            {
                serialized = serializer.SerializeToCSV<F>(instanceF);
            }

            stopwatch.Stop();

            TimeSpan dif1 = stopwatch.Elapsed;
            stopwatch.Reset();
            Console.WriteLine("Cериализация:\n");
            Console.WriteLine("Ушло времени на сериализацию в формат CSV (100 итераций): {0}", dif1);

            stopwatch.Start();

            for (int i = 0; i < 100_000; i++)
            {
                serialized = serializer.SerializeToCSV<F>(instanceF);
            }

            stopwatch.Stop();

            TimeSpan dif2 = stopwatch.Elapsed;
            stopwatch.Reset();
            Console.WriteLine("Ушло времени на сериализацию в формат CSV (100 000 итераций): {0}", dif2);
            
            Console.WriteLine("\nРазница во времени в {0} раз", dif2 / dif1);

            string res = "";

            stopwatch.Start();

            for (int i = 0; i < 100; i++)
            {
                res = JsonConvert.SerializeObject(instanceF);
            }

            stopwatch.Stop();

            dif1 = stopwatch.Elapsed;
            stopwatch.Reset();
            Console.WriteLine("\nУшло времени на сериализацию NewtonsoftJSON (100 итераций): {0}", dif1);

            stopwatch.Start();

            for (int i = 0; i < 100_000; i++)
            {
                res = JsonConvert.SerializeObject(instanceF);
            }

            stopwatch.Stop();

            dif2 = stopwatch.Elapsed;
            stopwatch.Reset();
            Console.WriteLine("Ушло времени на сериализацию NewtonsoftJSON (100 000 итераций): {0}", dif2);
            Console.WriteLine("\nРазница во времени в {0} раз", dif2 / dif1);

            Console.WriteLine("\nСериализация CSV: {0}", serialized);
            Console.WriteLine();
            Console.WriteLine("Сериализация NewtonsoftJSON: {0}", res);
            Console.WriteLine();
            Console.WriteLine("Десериализация:");
            Console.WriteLine();
            
            var deserialized = Serializer.DeserializeFromCSV<F>(serialized.ToString());

            stopwatch.Start();

            for (int i = 0; i < 100_000; i++)
            {
                deserialized = Serializer.DeserializeFromCSV<F>(serialized.ToString());
            }

            stopwatch.Stop();

            dif1 = stopwatch.Elapsed;
            stopwatch.Reset();
            Console.WriteLine("Ушло времени на десериализацию (100 000 итераций): {0}", dif1);
            
            var deserializeNewtonsoftJSON = JsonConvert.DeserializeObject<F>(res);
            
            stopwatch.Start();

            for (int i = 0; i < 100_000; i++)
            {
                deserializeNewtonsoftJSON = JsonConvert.DeserializeObject<F>(res);
            }


            stopwatch.Stop();

            dif1 = stopwatch.Elapsed;
            stopwatch.Reset();
            Console.WriteLine("Ушло времени на десериализацию NewtonsoftJSON (100 000 итераций): {0}", dif1);
            Console.WriteLine("\nРазница во времени в {0} раз", dif2 / dif1);


            Console.WriteLine("\nДесериализация моя:");
            deserialized.ShowFields();
            Console.WriteLine("\nДесериализация NewtonsoftJSON:");
            deserializeNewtonsoftJSON.ShowFields();

            Console.ReadKey();
        }
    }
}
